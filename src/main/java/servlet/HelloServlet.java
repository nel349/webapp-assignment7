package servlet;

import services.DataService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@WebServlet(
        name = "MyServlet", 
        urlPatterns = {"/openstack-meeting-analytics.html"}
    )
public class HelloServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        DataService a = new DataService();
        Map<String, List<String>> meetings = a.getAllMeetings();
        Map<String, String> to_display = new TreeMap<String, String>();
        for( String x : meetings.keySet()){
//            System.out.println("key = [" + x + "], value = [");

//            for(String y : meetings.get(x)){
//                System.out.println(y);
//            }
//            System.out.println(x +"  " +meetings.get(x).size());
            to_display.put(x, ""+meetings.get(x).size());
        }
//        System.out.println("]");
        request.setAttribute("to_display", to_display);
        request.getRequestDispatcher("openstack-meeting-analytics.jsp").forward(request, response);
    }
    
}
