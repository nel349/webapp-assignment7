package services;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by norman.lopez on 12/4/14.
 */
public class DataService {


    public Map<String, List<String>> getAllMeetings(){
        String url ="http://eavesdrop.openstack.org/meetings/";
        Document doc =null;
        try {
            doc = Jsoup.connect(url).get();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        List<String> all_meetings_htmls=  this.getQueryData(doc, true);
        List<String> all_meetings_names=  this.getQueryData(doc, false);
        Map<String, List<String> > result = new TreeMap<String, List<String>>();

        for(int i =0; i< all_meetings_htmls.size() ; ++i){
            List<String> years = getData(all_meetings_htmls.get(i), false);
            String meeting = all_meetings_names.get(i);
            List<String> meetings_years_total = new ArrayList<String>();
            for(String y : years){
                String url_y = url +"/" + meeting +"/" +y;
                meetings_years_total.addAll(getData(url_y, false));
            }
            result.put(all_meetings_names.get(i),meetings_years_total);
        }
        return result;
    }



    public List<String> getData(String link, boolean isLink){


        String url =link;
        Document doc =null;
        try {
            doc = Jsoup.connect(url).get();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        return this.getQueryData(doc, isLink);
    }

    public List<String> getQueryData(Document doc, boolean isLink){
        Elements links = doc.select("a[href]");
        List<String> all = new ArrayList<String>();
        int count =0;
        for (Element s_links : links) {
            String x;
            if(isLink){
                x = s_links.absUrl("href");
            }
            else{
                x = s_links.text();
            }

            if(count > 4 ){
//				String a = "<a href="+ x +">" + x +"</a>";
                all.add(x);
            }
            ++count;
        }
        return all;
    }
}
